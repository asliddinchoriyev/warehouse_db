package uz.pdp.app_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.app_warehouse.entity.input.Input;

@Repository
public interface InputRepository extends JpaRepository<Input, Integer> {
    boolean existsByCode(String code);
}
