package uz.pdp.app_warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.app_warehouse.entity.output.Output;

@Repository
public interface OutputRepository extends JpaRepository<Output, Integer> {
    boolean existsByCode(String code);
}
