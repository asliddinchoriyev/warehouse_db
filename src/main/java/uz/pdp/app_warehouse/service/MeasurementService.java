package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.measurement.Measurement;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.MeasurementRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MeasurementService {

    @Autowired
    MeasurementRepository measurementRepository;

    public Result add(Measurement measurement) {
        boolean existsByName = measurementRepository.existsByName(measurement.getName());

        if (existsByName)
            return new Result("This measurement already added", false);

        measurementRepository.save(measurement);

        return new Result("The measurement added", true);
    }

    public List<Measurement> getAll() {
        List<Measurement> all = measurementRepository.findAll();

        return all;
    }

    public Measurement getById(Integer id) {
        Optional<Measurement> optionalMeasurement = measurementRepository.findById(id);

        if (optionalMeasurement.isPresent()) {
            return optionalMeasurement.get();
        }
        return new Measurement();
    }

    public Result edit(Measurement comingMeasurement, Integer id) {

        Optional<Measurement> optionalMeasurement = measurementRepository.findById(id);

        if (optionalMeasurement.isEmpty())
            return new Result("The measurement not found", false);


        Measurement measurement = optionalMeasurement.get();
        measurement.setName(comingMeasurement.getName());

        measurementRepository.save(measurement);
        return new Result("The measurement edited", true);


    }

    public Result delete(Integer id) {
        Optional<Measurement> optionalMeasurement = measurementRepository.findById(id);

        if (optionalMeasurement.isEmpty())
            return new Result("The measurement not found", false);

        measurementRepository.deleteById(id);
        return new Result("The measurement deleted", true);
    }
}
