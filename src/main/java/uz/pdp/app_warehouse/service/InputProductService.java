package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.input.Input;
import uz.pdp.app_warehouse.entity.input.InputProduct;
import uz.pdp.app_warehouse.entity.measurement.Measurement;
import uz.pdp.app_warehouse.entity.product.Product;
import uz.pdp.app_warehouse.payload.InputProductDto;
import uz.pdp.app_warehouse.payload.OutputProductDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.InputProductRepository;
import uz.pdp.app_warehouse.repository.InputRepository;
import uz.pdp.app_warehouse.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class InputProductService {
    @Autowired
    InputProductRepository inputProductRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    InputRepository inputRepository;

    public Result add(InputProductDto inputProductDto) {
        InputProduct inputProduct = new InputProduct();

        inputProduct.setAmount(inputProductDto.getAmount());
        inputProduct.setPrice(inputProductDto.getPrice());
        inputProduct.setExpiredDate(inputProductDto.getExpiredDate());

        Optional<Product> optionalProduct = productRepository.findById(inputProductDto.getProductId());
        if (optionalProduct.isEmpty())
            return new Result("This product not found", false);

        inputProduct.setProduct(optionalProduct.get());

        Optional<Input> optionalInput = inputRepository.findById(inputProductDto.getInputId());
        if (optionalInput.isEmpty())
            return new Result("This input not found", false);

        inputProduct.setInput(optionalInput.get());

        inputProductRepository.save(inputProduct);

        return new Result("This inputProduct added", true);
    }

    public List<InputProduct> getAll() {
        List<InputProduct> all = inputProductRepository.findAll();

        return all;
    }

    public InputProduct getById(Integer id) {
        Optional<InputProduct> optionalInputProduct = inputProductRepository.findById(id);

        if (optionalInputProduct.isPresent()) {
            return optionalInputProduct.get();
        }
        return new InputProduct();
    }

    public Result edit(InputProductDto inputProductDto, Integer id) {

        Optional<InputProduct> optionalInputProduct = inputProductRepository.findById(id);

        if (optionalInputProduct.isEmpty())
            return new Result("The inputProduct not found", false);


        InputProduct inputProduct = optionalInputProduct.get();

        inputProduct.setAmount(inputProductDto.getAmount());
        inputProduct.setPrice(inputProductDto.getPrice());
        inputProduct.setExpiredDate(inputProductDto.getExpiredDate());

        Optional<Product> optionalProduct = productRepository.findById(inputProductDto.getProductId());
        if (optionalProduct.isEmpty())
            return new Result("This product not found", false);

        inputProduct.setProduct(optionalProduct.get());

        Optional<Input> optionalInput = inputRepository.findById(inputProductDto.getInputId());
        if (optionalInput.isEmpty())
            return new Result("This input not found", false);

        inputProduct.setInput(optionalInput.get());

        inputProductRepository.save(inputProduct);


        return new Result("The inputProduct edited", true);


    }

    public Result delete(Integer id) {
        Optional<InputProduct> optionalInputProduct = inputProductRepository.findById(id);

        if (optionalInputProduct.isEmpty())
            return new Result("The inputProduct not found", false);

        inputProductRepository.deleteById(id);
        return new Result("The inputProduct deleted", true);
    }
}
