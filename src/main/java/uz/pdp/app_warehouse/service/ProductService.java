package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.attachment.Attachment;
import uz.pdp.app_warehouse.entity.category.Category;
import uz.pdp.app_warehouse.entity.measurement.Measurement;
import uz.pdp.app_warehouse.entity.product.Product;
import uz.pdp.app_warehouse.payload.ProductDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.*;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    MeasurementRepository measurementRepository;

    public Result add(ProductDto productDto) {
        boolean existsByCode = productRepository.existsByCode(productDto.getCode());
        if (existsByCode)
            return new Result("This product already added", false);

        Product product = new Product();
        product.setName(productDto.getName());
        product.setCode(productDto.getName());

        Optional<Category> optionalCategory = categoryRepository.findById(productDto.getCategoryId());
        if (optionalCategory.isEmpty())
            return new Result("Category not found", false);

        product.setCategory(optionalCategory.get());

        Optional<Measurement> optionalMeasurement = measurementRepository.findById(productDto.getMeasurementId());
        if (optionalMeasurement.isEmpty())
            return new Result("Measurement not found", false);

        product.setMeasurement(optionalMeasurement.get());

        Optional<Attachment> optionalAttachment = attachmentRepository.findById(productDto.getPhotoId());

        if (optionalAttachment.isEmpty())
            return new Result("Photo not found", false);

        product.setPhoto(optionalAttachment.get());

        productRepository.save(product);

        return new Result("Product added", true);
    }

    public List<Product> getAll() {
        List<Product> all = productRepository.findAll();

        return all;
    }

    public Product getById(Integer id) {
        Optional<Product> optionalProduct = productRepository.findById(id);

        if (optionalProduct.isPresent()) {
            return optionalProduct.get();
        }
        return new Product();
    }

    public Result edit(ProductDto productDto, Integer id) {

        Optional<Product> optionalProduct = productRepository.findById(id);

        if (optionalProduct.isEmpty())
            return new Result("The product not found", false);

        Product product = optionalProduct.get();
        product.setName(productDto.getName());
        product.setCode(productDto.getName());

        Optional<Category> optionalCategory = categoryRepository.findById(productDto.getCategoryId());
        if (optionalCategory.isEmpty())
            return new Result("Category not found", false);

        product.setCategory(optionalCategory.get());

        Optional<Measurement> optionalMeasurement = measurementRepository.findById(productDto.getMeasurementId());
        if (optionalMeasurement.isEmpty())
            return new Result("Measurement not found", false);

        product.setMeasurement(optionalMeasurement.get());

        Optional<Attachment> optionalAttachment = attachmentRepository.findById(productDto.getPhotoId());

        if (optionalAttachment.isEmpty())
            return new Result("Photo not found", false);

        product.setPhoto(optionalAttachment.get());

        productRepository.save(product);

        return new Result("The product edited", true);


    }

    public Result delete(Integer id) {
        Optional<Product> optionalProduct = productRepository.findById(id);

        if (optionalProduct.isEmpty())
            return new Result("The product not found", false);

        productRepository.deleteById(id);
        return new Result("The product deleted", true);
    }
}
