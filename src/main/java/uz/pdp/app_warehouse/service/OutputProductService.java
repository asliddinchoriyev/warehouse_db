package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.input.Input;
import uz.pdp.app_warehouse.entity.input.InputProduct;
import uz.pdp.app_warehouse.entity.output.Output;
import uz.pdp.app_warehouse.entity.output.OutputProduct;
import uz.pdp.app_warehouse.entity.product.Product;
import uz.pdp.app_warehouse.payload.InputProductDto;
import uz.pdp.app_warehouse.payload.OutputProductDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.*;

import java.util.List;
import java.util.Optional;

@Service
public class OutputProductService {
    @Autowired
    OutputProductRepository outputProductRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    OutputRepository outputRepository;

    public Result add(OutputProductDto outputProductDto) {
        OutputProduct outputProduct = new OutputProduct();

        outputProduct.setAmount(outputProductDto.getAmount());
        outputProduct.setPrice(outputProductDto.getPrice());

        Optional<Product> optionalProduct = productRepository.findById(outputProductDto.getProductId());
        if (optionalProduct.isEmpty())
            return new Result("This product not found", false);

        outputProduct.setProduct(optionalProduct.get());

        Optional<Output> optionalOutput = outputRepository.findById(outputProductDto.getOutputId());
        if (optionalOutput.isEmpty())
            return new Result("This output not found", false);

        outputProduct.setOutput(optionalOutput.get());

        outputProductRepository.save(outputProduct);

        return new Result("This outputProduct added", true);
    }

    public List<OutputProduct> getAll() {
        List<OutputProduct> all = outputProductRepository.findAll();

        return all;
    }

    public OutputProduct getById(Integer id) {
        Optional<OutputProduct> optionalOutputProduct = outputProductRepository.findById(id);

        if (optionalOutputProduct.isPresent()) {
            return optionalOutputProduct.get();
        }
        return new OutputProduct();
    }

    public Result edit(OutputProductDto outputProductDto, Integer id) {

        Optional<OutputProduct> optionalOutputProduct = outputProductRepository.findById(id);

        if (optionalOutputProduct.isEmpty())
            return new Result("The outputProduct not found", false);


        OutputProduct outputProduct = optionalOutputProduct.get();


        outputProduct.setAmount(outputProductDto.getAmount());
        outputProduct.setPrice(outputProductDto.getPrice());

        Optional<Product> optionalProduct = productRepository.findById(outputProductDto.getProductId());
        if (optionalProduct.isEmpty())
            return new Result("This product not found", false);

        outputProduct.setProduct(optionalProduct.get());

        Optional<Output> optionalOutput = outputRepository.findById(outputProductDto.getOutputId());
        if (optionalOutput.isEmpty())
            return new Result("This output not found", false);

        outputProduct.setOutput(optionalOutput.get());

        outputProductRepository.save(outputProduct);
        return new Result("The inputProduct edited", true);


    }

    public Result delete(Integer id) {
        Optional<OutputProduct> optionalOutputProduct = outputProductRepository.findById(id);

        if (optionalOutputProduct.isEmpty())
            return new Result("The outputProduct not found", false);

        outputProductRepository.deleteById(id);
        return new Result("The outputProduct deleted", true);
    }
}
