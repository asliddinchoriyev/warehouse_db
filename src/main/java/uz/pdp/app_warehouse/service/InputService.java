package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.currency.Currency;
import uz.pdp.app_warehouse.entity.input.Input;
import uz.pdp.app_warehouse.entity.supplier.Supplier;
import uz.pdp.app_warehouse.entity.user.User;
import uz.pdp.app_warehouse.entity.warehouse.Warehouse;
import uz.pdp.app_warehouse.payload.InputDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.payload.UserDto;
import uz.pdp.app_warehouse.repository.*;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class InputService {
    @Autowired
    InputRepository inputRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    SupplierRepository supplierRepository;
    @Autowired
    CurrencyRepository currencyRepository;

    public Result add(InputDto inputDto) {
        boolean existsByCode = inputRepository.existsByCode(inputDto.getCode());

        if (existsByCode)
            return new Result("This input already added", false);

        Input input = new Input();
        input.setDate(inputDto.getDate());
        input.setFactureNumber(inputDto.getFactureNumber());
        input.setCode(inputDto.getCode());

        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(inputDto.getWarehouseId());
        if (optionalWarehouse.isEmpty())
            return new Result("This warehouse not found", false);

        input.setWarehouse(optionalWarehouse.get());

        Optional<Supplier> optionalSupplier = supplierRepository.findById(inputDto.getSupplierId());
        if (optionalSupplier.isEmpty())
            return new Result("This supplier not found", false);

        input.setSupplier(optionalSupplier.get());

        Optional<Currency> optionalCurrency = currencyRepository.findById(inputDto.getCurrencyId());
        if (optionalCurrency.isEmpty())
            return new Result("This currency not found", false);

        input.setCurrency(optionalCurrency.get());

        inputRepository.save(input);
        return new Result("The input added", true);
    }

    public List<Input> getAll() {
        List<Input> all = inputRepository.findAll();

        return all;
    }

    public Input getById(Integer id) {
        Optional<Input> optionalInput = inputRepository.findById(id);

        if (optionalInput.isPresent()) {
            return optionalInput.get();
        }
        return new Input();
    }

    public Result edit(InputDto inputDto, Integer id) {

        Optional<Input> optionalInput = inputRepository.findById(id);

        if (optionalInput.isEmpty())
            return new Result("The input not found", false);

        Input input = optionalInput.get();
        input.setDate(inputDto.getDate());
        input.setFactureNumber(inputDto.getFactureNumber());
        input.setCode(inputDto.getCode());

        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(inputDto.getWarehouseId());
        if (optionalWarehouse.isEmpty())
            return new Result("This warehouse not found", false);

        input.setWarehouse(optionalWarehouse.get());

        Optional<Supplier> optionalSupplier = supplierRepository.findById(inputDto.getSupplierId());
        if (optionalSupplier.isEmpty())
            return new Result("This supplier not found", false);

        input.setSupplier(optionalSupplier.get());

        Optional<Currency> optionalCurrency = currencyRepository.findById(inputDto.getCurrencyId());
        if (optionalCurrency.isEmpty())
            return new Result("This currency not found", false);

        input.setCurrency(optionalCurrency.get());

        inputRepository.save(input);

        return new Result("The input edited", true);


    }

    public Result delete(Integer id) {
        Optional<Input> optionalInput = inputRepository.findById(id);

        if (optionalInput.isEmpty())
            return new Result("The input not found", false);

        inputRepository.deleteById(id);
        return new Result("The input deleted", true);
    }
}
