package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.category.Category;
import uz.pdp.app_warehouse.entity.user.User;
import uz.pdp.app_warehouse.entity.warehouse.Warehouse;
import uz.pdp.app_warehouse.payload.CategoryDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.payload.UserDto;
import uz.pdp.app_warehouse.repository.CategoryRepository;
import uz.pdp.app_warehouse.repository.UserRepository;
import uz.pdp.app_warehouse.repository.WarehouseRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    WarehouseRepository warehouseRepository;

    public Result add(UserDto userDto) {
        boolean existsByPhoneNumber = userRepository.existsByPhoneNumber(userDto.getPhoneNumber());

        if (existsByPhoneNumber) {
            return new Result("This user already added", false);
        }

        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setPassword(userDto.getPassword());
        user.setCode(userDto.getCode());

        Set<Warehouse> warehouseSet = new HashSet<>();
        for (Integer warehouseId : userDto.getWarehouses()) {
            Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(warehouseId);
            if (optionalWarehouse.isPresent()) {
                warehouseSet.add(optionalWarehouse.get());
            }
        }

        user.setWarehouses(warehouseSet);

        userRepository.save(user);

        return new Result("The user added", true);
    }

    public List<User> getAll() {
        List<User> all = userRepository.findAll();

        return all;
    }

    public User getById(Integer id) {
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isPresent()) {
            return optionalUser.get();
        }
        return new User();
    }

    public Result edit(UserDto userDto, Integer id) {

        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isEmpty())
            return new Result("The user not found", false);

        User user = optionalUser.get();

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setPassword(userDto.getPassword());
        user.setCode(userDto.getCode());

        Set<Warehouse> warehouseSet = new HashSet<>();
        for (Integer warehouseId : userDto.getWarehouses()) {
            Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(warehouseId);
            if (optionalWarehouse.isPresent()) {
                warehouseSet.add(optionalWarehouse.get());
            }
        }

        user.setWarehouses(warehouseSet);

        userRepository.save(user);

        return new Result("The category edited", true);


    }

    public Result delete(Integer id) {
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isEmpty())
            return new Result("The user not found", false);

        userRepository.deleteById(id);
        return new Result("The user deleted", true);
    }
}
