package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.measurement.Measurement;
import uz.pdp.app_warehouse.entity.supplier.Supplier;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.MeasurementRepository;
import uz.pdp.app_warehouse.repository.SupplierRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SupplierService {
    @Autowired
    SupplierRepository supplierRepository;

    public Result add(Supplier supplier) {
        boolean existsByPhoneNumber = supplierRepository.existsByPhoneNumber(supplier.getPhoneNumber());

        if (existsByPhoneNumber)
            return new Result("This supplier already added", false);

        supplierRepository.save(supplier);

        return new Result("The supplier added", true);
    }

    public List<Supplier> getAll() {
        List<Supplier> all = supplierRepository.findAll();

        return all;
    }

    public Supplier getById(Integer id) {
        Optional<Supplier> optionalSupplier = supplierRepository.findById(id);

        if (optionalSupplier.isPresent()) {
            return optionalSupplier.get();
        }
        return new Supplier();
    }

    public Result edit(Supplier comingSupplier, Integer id) {

        Optional<Supplier> optionalSupplier = supplierRepository.findById(id);

        if (optionalSupplier.isEmpty())
            return new Result("The supplier not found", false);


        Supplier supplier = optionalSupplier.get();
        supplier.setName(comingSupplier.getName());
        supplier.setPhoneNumber(comingSupplier.getPhoneNumber());

        supplierRepository.save(supplier);
        return new Result("The supplier edited", true);


    }

    public Result delete(Integer id) {
        Optional<Supplier> optionalSupplier = supplierRepository.findById(id);

        if (optionalSupplier.isEmpty())
            return new Result("The supplier not found", false);

        supplierRepository.deleteById(id);
        return new Result("The supplier deleted", true);
    }
}
