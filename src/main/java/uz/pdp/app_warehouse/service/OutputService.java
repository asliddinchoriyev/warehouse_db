package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.client.Client;
import uz.pdp.app_warehouse.entity.currency.Currency;
import uz.pdp.app_warehouse.entity.input.Input;
import uz.pdp.app_warehouse.entity.output.Output;
import uz.pdp.app_warehouse.entity.supplier.Supplier;
import uz.pdp.app_warehouse.entity.warehouse.Warehouse;
import uz.pdp.app_warehouse.payload.InputDto;
import uz.pdp.app_warehouse.payload.OutputDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.*;

import java.util.List;
import java.util.Optional;

@Service
public class OutputService {
    @Autowired
    OutputRepository outputRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    CurrencyRepository currencyRepository;

    public Result add(OutputDto outputDto) {
        boolean existsByCode = outputRepository.existsByCode(outputDto.getCode());

        if (existsByCode)
            return new Result("This output already added", false);

        Output output = new Output();
        output.setDate(outputDto.getDate());
        output.setFactureNumber(outputDto.getFactureNumber());
        output.setCode(outputDto.getCode());

        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(outputDto.getWarehouseId());
        if (optionalWarehouse.isEmpty())
            return new Result("This warehouse not found", false);

        output.setWarehouse(optionalWarehouse.get());

        Optional<Client> optionalClient = clientRepository.findById(outputDto.getClientId());
        if (optionalClient.isEmpty())
            return new Result("This client not found", false);

        output.setClient(optionalClient.get());

        Optional<Currency> optionalCurrency = currencyRepository.findById(outputDto.getCurrencyId());
        if (optionalCurrency.isEmpty())
            return new Result("This currency not found", false);

        output.setCurrency(optionalCurrency.get());

        outputRepository.save(output);
        return new Result("The output added", true);

    }

    public List<Output> getAll() {
        List<Output> all = outputRepository.findAll();

        return all;
    }

    public Output getById(Integer id) {
        Optional<Output> optionalOutput = outputRepository.findById(id);

        if (optionalOutput.isPresent()) {
            return optionalOutput.get();
        }
        return new Output();
    }

    public Result edit(OutputDto outputDto, Integer id) {

        Optional<Output> optionalOutput = outputRepository.findById(id);

        if (optionalOutput.isEmpty())
            return new Result("The output not found", false);

        Output output = optionalOutput.get();
        output.setDate(outputDto.getDate());
        output.setFactureNumber(outputDto.getFactureNumber());
        output.setCode(outputDto.getCode());

        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(outputDto.getWarehouseId());
        if (optionalWarehouse.isEmpty())
            return new Result("This warehouse not found", false);

        output.setWarehouse(optionalWarehouse.get());

        Optional<Client> optionalClient = clientRepository.findById(outputDto.getClientId());
        if (optionalClient.isEmpty())
            return new Result("This client not found", false);

        output.setClient(optionalClient.get());

        Optional<Currency> optionalCurrency = currencyRepository.findById(outputDto.getCurrencyId());
        if (optionalCurrency.isEmpty())
            return new Result("This currency not found", false);

        output.setCurrency(optionalCurrency.get());

        outputRepository.save(output);
        return new Result("The output edited", true);


    }

    public Result delete(Integer id) {
        Optional<Output> optionalOutput = outputRepository.findById(id);

        if (optionalOutput.isEmpty())
            return new Result("The output not found", false);

        outputRepository.deleteById(id);
        return new Result("The output deleted", true);
    }
}
