package uz.pdp.app_warehouse.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.client.Client;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.ClientRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    ClientRepository clientRepository;

    public Result add(Client client) {
        boolean existsByPhoneNumber = clientRepository.existsByPhoneNumber(client.getPhoneNumber());

        if (existsByPhoneNumber)
            return new Result("This client already added", false);

        clientRepository.save(client);

        return new Result("The client added", true);
    }

    public List<Client> getAll() {
        List<Client> all = clientRepository.findAll();

        return all;
    }

    public Client getById(Integer id) {
        Optional<Client> optionalClient = clientRepository.findById(id);

        if (optionalClient.isPresent()) {
            return optionalClient.get();
        }
        return new Client();
    }

    public Result edit(Client comingClient, Integer id) {

        Optional<Client> optionalClient = clientRepository.findById(id);

        if (optionalClient.isEmpty())
            return new Result("The supplier not found", false);


        Client client = optionalClient.get();
        client.setName(comingClient.getName());
        client.setPhoneNumber(comingClient.getPhoneNumber());

        clientRepository.save(client);
        return new Result("The client edited", true);


    }

    public Result delete(Integer id) {
        Optional<Client> optionalClient = clientRepository.findById(id);

        if (optionalClient.isEmpty())
            return new Result("The client not found", false);

        clientRepository.deleteById(id);
        return new Result("The client deleted", true);
    }
}
