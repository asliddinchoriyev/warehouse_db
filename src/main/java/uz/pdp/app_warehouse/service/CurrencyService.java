package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.currency.Currency;
import uz.pdp.app_warehouse.entity.measurement.Measurement;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.CurrencyRepository;
import uz.pdp.app_warehouse.repository.MeasurementRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CurrencyService {
    @Autowired
    CurrencyRepository currencyRepository;

    public Result add(Currency currency) {
        boolean existsByName = currencyRepository.existsByName(currency.getName());

        if (existsByName)
            return new Result("This currency already added", false);

        currencyRepository.save(currency);

        return new Result("The currency added", true);
    }

    public List<Currency> getAll() {
        List<Currency> all = currencyRepository.findAll();

        return all;
    }

    public Currency getById(Integer id) {
        Optional<Currency> optionalCurrency = currencyRepository.findById(id);

        if (optionalCurrency.isPresent()) {
            return optionalCurrency.get();
        }
        return new Currency();
    }

    public Result edit(Currency comingCurrency, Integer id) {

        Optional<Currency> optionalCurrency = currencyRepository.findById(id);

        if (optionalCurrency.isEmpty())
            return new Result("The currency not found", false);


        Currency currency = optionalCurrency.get();
        currency.setName(comingCurrency.getName());

        currencyRepository.save(currency);
        return new Result("The currency edited", true);


    }

    public Result delete(Integer id) {
        Optional<Currency> optionalCurrency = currencyRepository.findById(id);

        if (optionalCurrency.isEmpty())
            return new Result("The currency not found", false);

        currencyRepository.deleteById(id);
        return new Result("The currency deleted", true);
    }
}
