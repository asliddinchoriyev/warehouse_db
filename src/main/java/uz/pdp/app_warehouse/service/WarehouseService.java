package uz.pdp.app_warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.app_warehouse.entity.measurement.Measurement;
import uz.pdp.app_warehouse.entity.warehouse.Warehouse;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.repository.MeasurementRepository;
import uz.pdp.app_warehouse.repository.WarehouseRepository;

import java.util.List;
import java.util.Optional;

@Service
public class WarehouseService {
    @Autowired
    WarehouseRepository warehouseRepository;

    public Result add(Warehouse warehouse) {
        boolean existsByName = warehouseRepository.existsByName(warehouse.getName());

        if (existsByName)
            return new Result("This warehouse already added", false);

        warehouseRepository.save(warehouse);

        return new Result("The warehouse added", true);
    }

    public List<Warehouse> getAll() {
        List<Warehouse> all = warehouseRepository.findAll();

        return all;
    }

    public Warehouse getById(Integer id) {
        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(id);

        if (optionalWarehouse.isPresent()) {
            return optionalWarehouse.get();
        }
        return new Warehouse();
    }

    public Result edit(Warehouse comingWarehouse, Integer id) {

        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(id);

        if (optionalWarehouse.isEmpty())
            return new Result("The warehouse not found", false);


        Warehouse warehouse = optionalWarehouse.get();
        warehouse.setName(comingWarehouse.getName());

        warehouseRepository.save(warehouse);
        return new Result("The warehouse edited", true);


    }

    public Result delete(Integer id) {
        Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(id);

        if (optionalWarehouse.isEmpty())
            return new Result("The warehouse not found", false);

        warehouseRepository.deleteById(id);
        return new Result("The warehouse deleted", true);
    }
}
