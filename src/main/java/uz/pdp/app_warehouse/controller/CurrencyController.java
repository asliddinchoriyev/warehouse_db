package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.currency.Currency;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.CurrencyService;

import java.util.List;

@RestController
@RequestMapping(value = "/currency")
public class CurrencyController {
    @Autowired
    CurrencyService currencyService;

    @PostMapping
    public Result add(@RequestBody Currency currency) {
        Result result = currencyService.add(currency);

        return result;
    }

    @GetMapping
    public List<Currency> getAll() {
        List<Currency> all = currencyService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Currency getById(@PathVariable Integer id) {
        Currency currency = currencyService.getById(id);

        return currency;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody Currency currency, @PathVariable Integer id) {
        Result result = currencyService.edit(currency, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = currencyService.delete(id);

        return result;
    }
}
