package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.input.Input;
import uz.pdp.app_warehouse.entity.output.Output;
import uz.pdp.app_warehouse.payload.InputDto;
import uz.pdp.app_warehouse.payload.OutputDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.InputService;
import uz.pdp.app_warehouse.service.OutputService;

import java.util.List;

@RestController
@RequestMapping(value = "/output")
public class OutputController {
    @Autowired
    OutputService outputService;

    @PostMapping
    public Result add(@RequestBody OutputDto outputDto) {
        Result result = outputService.add(outputDto);

        return result;
    }

    @GetMapping
    public List<Output> getAll() {
        List<Output> all = outputService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Output getById(@PathVariable Integer id) {
        Output output = outputService.getById(id);

        return output;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody OutputDto outputDto, @PathVariable Integer id) {
        Result result = outputService.edit(outputDto, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = outputService.delete(id);

        return result;
    }
}
