package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.supplier.Supplier;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.SupplierService;

import java.util.List;

@RestController
@RequestMapping(value = "/supplier")
public class SupplierController {
    @Autowired
    SupplierService supplierService;

    @PostMapping
    public Result add(@RequestBody Supplier supplier) {
        Result result = supplierService.add(supplier);

        return result;
    }

    @GetMapping
    public List<Supplier> getAll() {
        List<Supplier> all = supplierService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Supplier getById(@PathVariable Integer id) {
        Supplier supplier = supplierService.getById(id);

        return supplier;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody Supplier supplier, @PathVariable Integer id) {
        Result result = supplierService.edit(supplier, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = supplierService.delete(id);

        return result;
    }
}
