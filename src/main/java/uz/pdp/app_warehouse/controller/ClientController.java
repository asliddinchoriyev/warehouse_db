package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;;
import uz.pdp.app_warehouse.entity.client.Client;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.ClientService;

import java.util.List;

@RestController
@RequestMapping(value = "/client")
public class ClientController {
    @Autowired
    ClientService clientService;

    @PostMapping
    public Result add(@RequestBody Client client) {
        Result result = clientService.add(client);

        return result;
    }

    @GetMapping
    public List<Client> getAll() {
        List<Client> all = clientService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Client getById(@PathVariable Integer id) {
        Client client = clientService.getById(id);

        return client;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody Client client, @PathVariable Integer id) {
        Result result = clientService.edit(client, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = clientService.delete(id);

        return result;
    }
}
