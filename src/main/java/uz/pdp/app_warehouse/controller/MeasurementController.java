package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.measurement.Measurement;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.MeasurementService;

import java.util.List;

@RestController
@RequestMapping(value = "/measurement")
public class MeasurementController {

    @Autowired
    MeasurementService measurementService;

    @PostMapping
    public Result add(@RequestBody Measurement measurement) {
        Result result = measurementService.add(measurement);

        return result;
    }

    @GetMapping
    public List<Measurement> getAll() {
        List<Measurement> all = measurementService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Measurement getById(@PathVariable Integer id) {
        Measurement measurement = measurementService.getById(id);

        return measurement;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody Measurement measurement, @PathVariable Integer id) {
        Result result = measurementService.edit(measurement, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = measurementService.delete(id);

        return result;
    }
}
