package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.product.Product;
import uz.pdp.app_warehouse.payload.ProductDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.ProductService;

import java.util.List;

@RestController
@RequestMapping(value = "/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @PostMapping
    public Result add(@RequestBody ProductDto productDto) {
        Result result = productService.add(productDto);

        return result;
    }

    @GetMapping
    public List<Product> getAll() {
        List<Product> all = productService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Product getById(@PathVariable Integer id) {
        Product product = productService.getById(id);

        return product;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody ProductDto productDto, @PathVariable Integer id) {
        Result result = productService.edit(productDto, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = productService.delete(id);

        return result;
    }
}
