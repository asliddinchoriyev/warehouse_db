package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.category.Category;
import uz.pdp.app_warehouse.payload.CategoryDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.CategoryService;

import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @PostMapping
    public Result add(@RequestBody CategoryDto categoryDto) {
        Result result = categoryService.add(categoryDto);

        return result;
    }

    @GetMapping
    public List<Category> getAll() {
        List<Category> all = categoryService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Category getById(@PathVariable Integer id) {
        Category category = categoryService.getById(id);

        return category;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody CategoryDto categoryDto, @PathVariable Integer id) {
        Result result = categoryService.edit(categoryDto, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = categoryService.delete(id);

        return result;
    }
}
