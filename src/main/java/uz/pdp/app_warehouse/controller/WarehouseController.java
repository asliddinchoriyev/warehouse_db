package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.warehouse.Warehouse;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.WarehouseService;

import java.util.List;

@RestController
@RequestMapping(value = "/warehouse")
public class WarehouseController {
    @Autowired
    WarehouseService warehouseService;

    @PostMapping
    public Result add(@RequestBody Warehouse warehouse) {
        Result result = warehouseService.add(warehouse);

        return result;
    }

    @GetMapping
    public List<Warehouse> getAll() {
        List<Warehouse> all = warehouseService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Warehouse getById(@PathVariable Integer id) {
        Warehouse warehouse = warehouseService.getById(id);

        return warehouse;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody Warehouse warehouse, @PathVariable Integer id) {
        Result result = warehouseService.edit(warehouse, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = warehouseService.delete(id);

        return result;
    }

}
