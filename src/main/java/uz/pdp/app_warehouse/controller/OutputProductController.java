package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.input.InputProduct;
import uz.pdp.app_warehouse.entity.output.OutputProduct;
import uz.pdp.app_warehouse.payload.InputProductDto;
import uz.pdp.app_warehouse.payload.OutputProductDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.InputProductService;
import uz.pdp.app_warehouse.service.OutputProductService;

import java.util.List;

;

@RestController
@RequestMapping(value = "/outputProduct")
public class OutputProductController {
    @Autowired
    OutputProductService outputProductService;

    @PostMapping
    public Result add(@RequestBody OutputProductDto outputProductDto) {
        Result result = outputProductService.add(outputProductDto);

        return result;
    }

    @GetMapping
    public List<OutputProduct> getAll() {
        List<OutputProduct> all = outputProductService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public OutputProduct getById(@PathVariable Integer id) {
        OutputProduct outputProduct = outputProductService.getById(id);

        return outputProduct;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody OutputProductDto outputProductDto, @PathVariable Integer id) {
        Result result = outputProductService.edit(outputProductDto, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = outputProductService.delete(id);

        return result;
    }
}
