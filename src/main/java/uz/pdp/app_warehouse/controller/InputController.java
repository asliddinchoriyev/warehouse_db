package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.input.Input;
import uz.pdp.app_warehouse.payload.InputDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.InputService;

import java.util.List;

@RestController
@RequestMapping(value = "/input")
public class InputController {
    @Autowired
    InputService inputService;

    @PostMapping
    public Result add(@RequestBody InputDto inputDto) {
        Result result = inputService.add(inputDto);

        return result;
    }

    @GetMapping
    public List<Input> getAll() {
        List<Input> all = inputService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public Input getById(@PathVariable Integer id) {
        Input input = inputService.getById(id);

        return input;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody InputDto inputDto, @PathVariable Integer id) {
        Result result = inputService.edit(inputDto, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = inputService.delete(id);

        return result;
    }
}
