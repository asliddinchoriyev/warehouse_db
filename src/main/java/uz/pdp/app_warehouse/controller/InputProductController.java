package uz.pdp.app_warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.app_warehouse.entity.input.InputProduct;
import uz.pdp.app_warehouse.payload.InputProductDto;
import uz.pdp.app_warehouse.payload.Result;
import uz.pdp.app_warehouse.service.InputProductService;;

import java.util.List;

@RestController
@RequestMapping(value = "/inputProduct")
public class InputProductController {
    @Autowired
    InputProductService inputProductService;

    @PostMapping
    public Result add(@RequestBody InputProductDto inputProductDto) {
        Result result = inputProductService.add(inputProductDto);

        return result;
    }

    @GetMapping
    public List<InputProduct> getAll() {
        List<InputProduct> all = inputProductService.getAll();

        return all;
    }

    @GetMapping(value = "/{id}")
    public InputProduct getById(@PathVariable Integer id) {
        InputProduct inputProduct = inputProductService.getById(id);

        return inputProduct;
    }

    @PutMapping(value = "/{id}")
    public Result edit(@RequestBody InputProductDto inputProductDto, @PathVariable Integer id) {
        Result result = inputProductService.edit(inputProductDto, id);

        return result;
    }

    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Integer id) {
        Result result = inputProductService.delete(id);

        return result;
    }
}
